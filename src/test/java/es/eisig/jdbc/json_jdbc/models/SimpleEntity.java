package es.eisig.jdbc.json_jdbc.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class SimpleEntity {
	@Id
	@GeneratedValue
	Long id;
}
