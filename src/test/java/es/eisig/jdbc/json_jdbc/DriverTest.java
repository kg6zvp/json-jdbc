package es.eisig.jdbc.json_jdbc;

import es.eisig.jdbc.json_jdbc.models.SimpleEntity;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.Driver;
import java.util.Iterator;
import java.util.ServiceLoader;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class DriverTest {
	@Test
	public void serviceShouldLoad() {
		ServiceLoader<Driver> driverLoader = ServiceLoader.load(Driver.class);
		Iterator<Driver> driverIterator = driverLoader.iterator();

		assertTrue(driverIterator.hasNext());

		assertTrue(driverIterator.next() instanceof es.eisig.jdbc.json_jdbc.Driver);
	}

	@Test
	public void createEntityManagerFactory() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");
		EntityManager em = emf.createEntityManager();
		em.persist(new SimpleEntity());
	}
}
