package es.eisig.jdbc.json_jdbc;

import es.eisig.jdbc.json_jdbc.err.UnsupportedDriverException;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.kohsuke.MetaInfServices;

import java.sql.Connection;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

@MetaInfServices
public class Driver implements java.sql.Driver {
	public Driver() {
		System.out.println("Driver#Driver()");
	}

    public Connection connect(String connectionUrl, Properties properties) throws SQLException {
        //TODO: define usage of connection string and properties
		System.out.println(String.format("Driver#connect(%s, %s)", connectionUrl, properties));
        return FileConnection.builder()
				.url(connectionUrl)
				.props(properties)
				.build();
    }

    public boolean acceptsURL(String s) throws SQLException {
        throw new UnsupportedDriverException(String.format("Driver#acceptsUrl(%s)", s));
    }

    public DriverPropertyInfo[] getPropertyInfo(String s, Properties properties) throws SQLException {
    	String msg = String.format("Driver#getPropertyInfo(%s, %s)", s, properties.toString());
        throw new UnsupportedDriverException(msg);
    }

    public int getMajorVersion() {
		System.out.println("Driver#getMajorVersion()");
        return 1;
    }

    public int getMinorVersion() {
		System.out.println("Driver#getMinorVersion()");
        return 0;
    }

    public boolean jdbcCompliant() {
		System.out.println("Driver#jdbcCompliant()");
        return true;
    }

    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        throw new UnsupportedDriverException("Driver#getParentLogger()");
    }
}
