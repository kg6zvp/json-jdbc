package es.eisig.jdbc.json_jdbc.err;

public class UnsupportedDriverException extends UnsupportedOperationException {
	public UnsupportedDriverException(String msg) {
		super(msg);
		System.out.println(getStackTrace().toString());
	}
}
