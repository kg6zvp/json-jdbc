package es.eisig.jdbc.json_jdbc;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.sql.SQLException;
import java.sql.SQLXML;

@Data
@Builder
@NoArgsConstructor
//@AllArgsConstructor
public class JsonSQLXML implements SQLXML {
	@Override
	public void free() throws SQLException {

	}

	@Override
	public InputStream getBinaryStream() throws SQLException {
		return null;
	}

	@Override
	public OutputStream setBinaryStream() throws SQLException {
		return null;
	}

	@Override
	public Reader getCharacterStream() throws SQLException {
		return null;
	}

	@Override
	public Writer setCharacterStream() throws SQLException {
		return null;
	}

	@Override
	public String getString() throws SQLException {
		return null;
	}

	@Override
	public void setString(String s) throws SQLException {

	}

	@Override
	public <T extends Source> T getSource(Class<T> aClass) throws SQLException {
		return null;
	}

	@Override
	public <T extends Result> T setResult(Class<T> aClass) throws SQLException {
		return null;
	}
}
