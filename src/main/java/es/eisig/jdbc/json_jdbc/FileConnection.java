package es.eisig.jdbc.json_jdbc;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FileConnection implements Connection {
    Boolean autoCommit;
    String url;
    Properties props;

    public Statement createStatement() throws SQLException {
        //TODO
		System.out.println("FileConnection#createStatement()");
        return JsonStatement.builder().build();
    }

    public PreparedStatement prepareStatement(String s) throws SQLException {
        //TODO
		System.out.printf("FileConnection#prepareStatement(%s)\n", s);
        return JsonPreparedStatement.builder().build();
    }

    public CallableStatement prepareCall(String s) throws SQLException {
        //TODO
		System.out.printf("FileConnection#prepareCall(%s)\n", s);
        return null;
    }

    public String nativeSQL(String s) throws SQLException {
        //TODO
		System.out.printf("FileConnection#nativeSQL(%s)\n", s);
        return null;
    }

    public void setAutoCommit(boolean autoCommit) throws SQLException {
    	System.out.printf("FileConnection#setAutoCommit(%b)\n", autoCommit);
        this.autoCommit = autoCommit;
    }

    public boolean getAutoCommit() throws SQLException {
        //TODO
		System.out.println("FileConnection#getAutoCommit()");
        return this.autoCommit != null ? this.autoCommit : true;
    }

    public void commit() throws SQLException {
		System.out.println("FileConnection#commit()");
		throw new SQLException();
    }

    public void rollback() throws SQLException {
		System.out.println("FileConnection#rollback()");
		throw new SQLException();
    }

    public void close() throws SQLException {
		System.out.println("FileConnection#close()");
		throw new SQLException();
	}

    public boolean isClosed() throws SQLException {
		System.out.println("FileConnection#isClosed()");
    	throw new SQLException();
    }

    public DatabaseMetaData getMetaData() throws SQLException {
		System.out.println("FileConnection#getMetaData()");
        return null;
    }

    public void setReadOnly(boolean b) throws SQLException {
		System.out.printf("FileConnection#setReadOnly(%b)\n", b);
    }

    public boolean isReadOnly() throws SQLException {
		System.out.println("FileConnection#isReadOnly()");
        return false;
    }

    public void setCatalog(String s) throws SQLException {
		System.out.printf("FileConnection#setCatalog(%s)\n", s);
    }

    public String getCatalog() throws SQLException {
		System.out.println("FileConnection#getCatalog()");
        return null;
    }

    public void setTransactionIsolation(int i) throws SQLException {
		System.out.printf("FileConnection#setTransactionIsolation(int = %d)\n", i);
    }

    public int getTransactionIsolation() throws SQLException {
		System.out.println("FileConnection#getTransactionIsolation()");
        return 0;
    }

    public SQLWarning getWarnings() throws SQLException {
		System.out.println("FileConnection#getWarnings()");
        return null;
    }

    public void clearWarnings() throws SQLException {
		System.out.println("FileConnection#clearWarnings()");
    }

    public Statement createStatement(int i, int i1) throws SQLException {
		System.out.printf("FileConnection#createStatement(%d, %d)\n", i, i1);
        return null;
    }

    public PreparedStatement prepareStatement(String s, int i, int i1) throws SQLException {
		System.out.printf("FileConnection#prepareStatement(%s, %d, %d)\n", s, i, i1);
        return null;
    }

    public CallableStatement prepareCall(String s, int i, int i1) throws SQLException {
		System.out.printf("FileConnection#prepareCall(%s, %d, %d)\n", s, i, i1);
        return null;
    }

    public Map<String, Class<?>> getTypeMap() throws SQLException {
		System.out.println("FileConnection#getTypeMap()");
        return null;
    }

    public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
		System.out.printf("FileConnection#setTypeMap(%s)\n", map.toString());
    }

    public void setHoldability(int i) throws SQLException {
		System.out.printf("Fileconnection#setHoldability(%d)\n", i);
    }

    public int getHoldability() throws SQLException {
		System.out.println("Fileconnection#getHoldability()");
        return 0;
    }

    public Savepoint setSavepoint() throws SQLException {
		System.out.println("FileConnection#setSavepoint()");
        return null;
    }

    public Savepoint setSavepoint(String s) throws SQLException {
		System.out.printf("FileConnection#setSavepoint(%s)\n", s);
        return null;
    }

    public void rollback(Savepoint savepoint) throws SQLException {
		System.out.printf("FileConnection#rollback(%s)\n", savepoint.toString());
    }

    public void releaseSavepoint(Savepoint savepoint) throws SQLException {
		System.out.printf("FileConnection#releaseSavepoint(%s)\n", savepoint.toString());
    }

    public Statement createStatement(int i, int i1, int i2) throws SQLException {
		System.out.printf("FileConnection#createStatement(%d, %d, %d)\n", i, i1, i2);
        return null;
    }

    public PreparedStatement prepareStatement(String s, int i, int i1, int i2) throws SQLException {
		System.out.printf("FileConnection#prepareStatement(%s, %d, %d, %d)\n", s, i, i1, i2);
        return null;
    }

    public CallableStatement prepareCall(String s, int i, int i1, int i2) throws SQLException {
		System.out.printf("FileConnection#prepareCall(%s, %d, %d, %d)\n", s, i, i1, i2);
        return null;
    }

    public PreparedStatement prepareStatement(String s, int i) throws SQLException {
		System.out.printf("FileConnection#prepareStatement(%s, %d)\n", s, i);
        return null;
    }

    public PreparedStatement prepareStatement(String s, int[] ints) throws SQLException {
		System.out.printf("FileConnection#prepareStatement(%s, %s)\n", s, ints.toString());
        return null;
    }

    public PreparedStatement prepareStatement(String s, String[] strings) throws SQLException {
		System.out.printf("FileConnection#prepareStatement(%s, %s)\n", s, strings.toString());
        return null;
    }

    public Clob createClob() throws SQLException {
		System.out.println("FileConnection#createClob()");
        return null;
    }

    public Blob createBlob() throws SQLException {
		System.out.println("FileConnection#createBlob()");
        return null;
    }

    public NClob createNClob() throws SQLException {
		System.out.println("FileConnection#createNClob()");
        return null;
    }

    public SQLXML createSQLXML() throws SQLException {
		System.out.println("FileConnection#createSQLXML()");
        return null;
    }

    public boolean isValid(int i) throws SQLException {
		System.out.printf("FileConnection#isValid(%d)\n", i);
        return false;
    }

    public void setClientInfo(String s, String s1) throws SQLClientInfoException {
		System.out.printf("FileConnection#setClientInfo(%s, %s)\n", s, s1);
    }

    public void setClientInfo(Properties properties) throws SQLClientInfoException {
		System.out.printf("FileConnection#setClientInfo(%s)\n", properties.toString());
    }

    public String getClientInfo(String s) throws SQLException {
		System.out.printf("FileConnection#getClientInfo(%s)\n", s);
        return null;
    }

    public Properties getClientInfo() throws SQLException {
		System.out.println("FileConnection#getClientInfo()");
        return null;
    }

    public Array createArrayOf(String s, Object[] objects) throws SQLException {
		System.out.printf("FileConnection#createArrayOf(%s, %s)\n", s, objects.toString());
        return null;
    }

    public Struct createStruct(String s, Object[] objects) throws SQLException {
		System.out.printf("FileConnection#createStruct(%s, %s)\n", s, objects.toString());
        return null;
    }

    public void setSchema(String s) throws SQLException {
		System.out.printf("FileConnection#setSchema(%s)\n", s);
    }

    public String getSchema() throws SQLException {
		System.out.println("FileConnection#getSchema()");
        return null;
    }

    public void abort(Executor executor) throws SQLException {
		System.out.printf("FileConnection#abort(%s)\n", executor.toString());
    }

    public void setNetworkTimeout(Executor executor, int i) throws SQLException {
		System.out.printf("FileConnection#setNetworkTimeout(%s, %d)\n", executor.toString(), i);
    }

    public int getNetworkTimeout() throws SQLException {
		System.out.println("FileConnection#getNetworkTimeout()");
        return 0;
    }

    public <T> T unwrap(Class<T> aClass) throws SQLException {
		System.out.printf("FileConnection#unwrap(%s)\n", aClass.getPackage() + "." + aClass.getSimpleName());
        return null;
    }

    public boolean isWrapperFor(Class<?> aClass) throws SQLException {
		System.out.printf("FileConnection#isWrapperFor(%s)\n", aClass.getPackage() + "." + aClass.getSimpleName());
        return false;
    }
}
