package es.eisig.jdbc.json_jdbc;

import lombok.*;

import java.sql.SQLException;
import java.sql.Savepoint;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JsonSavepoint implements Savepoint {
	@Setter
	String savepointName;
	@Setter
	int savepointId;

	@Override
	public int getSavepointId() throws SQLException {
		return savepointId;
	}

	@Override
	public String getSavepointName() throws SQLException {
		return savepointName;
	}
}
